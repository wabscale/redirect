# Python
MAIN_NAME=app.py
ENV_NAME=venv
PYTHON_VERSION=`which python3`

.PHONY: run setup

all: deploy

deploy:
	docker-compose build
	docker-compose push

setup:
	if [ -d ${ENV_NAME} ]; then \
		rm -rf ${ENV_NAME}; \
	fi
	if [ -a requirements.txt ]; then \
		touch requirements.txt; \
	fi
	which virtualenv && pip install virtualenv || true
	virtualenv -p ${PYTHON_VERSION} ${ENV_NAME}
	./${ENV_NAME}/bin/pip install -r requirements.txt

run:
	if [ ! -d ${ENV_NAME} ]; then \
		make setup; \
	fi
	DEST=http://example.com/ ./${ENV_NAME}/bin/python ${MAIN_NAME}

clean:
	if [ -d ${ENV_NAME} ]; then \
		rm -rf ${ENV_NAME}; \
	fi
	if [ -n "`find . -name __pycache__`" ]; then \
		rm -rf `find . -name __pycache__`; \
	fi
