from flask import Flask, redirect
from os import environ
import os

app=Flask(__name__)

dest=environ.get('DEST')
dest = 'http://' + dest if not (dest.startswith('https://') or dest.startswith('http://')) else dest
dest = dest + '/' if not dest.endswith('/') else dest

@app.route('/')
@app.route('/<path:path>')
def index(path=''):
    return redirect(dest + path)


if __name__ == "__main__":
    app.run('0.0.0.0', 5000)

