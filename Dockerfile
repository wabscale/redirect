FROM python:3.7-alpine

WORKDIR /opt/app

RUN pip3 install flask gunicorn

COPY app.py .

CMD gunicorn -b 0.0.0.0:80 -w 2 app:app
