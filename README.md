# Redirect

About as simple as it gets. Just give it a place to redirect to.

## To Get

```bash
docker pull jmc1283/redirect
```

## To Use
```bash
docker run -p 80:80 -e DEST=https://reddit.com jmc1283/redirect
```
